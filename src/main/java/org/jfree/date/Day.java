package org.jfree.date;

import java.text.DateFormatSymbols;
import java.util.Calendar;

public enum Day {
    MONDAY(Calendar.MONDAY),
    TUESDAY(Calendar.TUESDAY),
    WEDNESDAY(Calendar.WEDNESDAY),
    THURSDAY(Calendar.THURSDAY),
    FRIDAY(Calendar.FRIDAY),
    SATURDAY(Calendar.SATURDAY),
    SUNDAY(Calendar.SUNDAY);

    private final int index;
    private static DateFormatSymbols dateSymbols = new DateFormatSymbols();

    Day(int day) {
        index = day;
    }

    public int toInt() {
        return index;
    }

    public static Day fromInt(int index) throws IllegalArgumentException {
        for (Day d : Day.values())
            if (d.index == index)
                return d;
        throw new IllegalArgumentException(
                String.format("Illegal day index: %d.", index));
    }

    public static Day parse(String s) {
        String[] shortWeekdayNames
                = dateSymbols.getShortWeekdays();
        String[] weekDayNames = dateSymbols.getWeekdays();

        s = s.trim();
        for (Day day : values()) {
            if (s.equalsIgnoreCase(shortWeekdayNames[day.index]) ||
                    s.equalsIgnoreCase(weekDayNames[day.index]))
                return day;
        }
        throw new IllegalArgumentException(
                String.format("%s is not a valid weekday string", s));
    }

    public static String toString(Day weekday) {
        return dateSymbols.getWeekdays()[weekday.index];
    }
}

